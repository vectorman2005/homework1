package models;

import java.util.Objects;

/**
 * Объект тикета
 */
public class Ticket {
    private Integer id;

    private Integer queue;

    private String title;

    private String description;

    private String dueDate;

    private Integer priority;

    private String mail;


    public Integer getQueue() {
        return queue;
    }

    public void setQueue(Integer queue) {
        this.queue = queue;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public void setPriority(String priority) {
        String converted = priority.replaceAll("\\D", "");
        this.priority = Integer.parseInt(converted);
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return getPriority() == ticket.getPriority() && getTitle().contains(ticket.getTitle()) && Objects.equals(getDescription(), ticket.getDescription()) && Objects.equals(getMail(), ticket.getMail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getDescription(), getPriority(), getMail());
    }
}
