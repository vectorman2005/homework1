package pages;

import models.Ticket;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/** Страница создания тикета */
public class CreateTicketPage extends HelpdeskBasePage {

    @FindBy(xpath = "//select[@id = 'id_queue']")
    private WebElement queueList;

    @FindBy(xpath = "//select[@id = 'id_queue']//option[@value = '1']")
    private WebElement queueValue;

    @FindBy(id = "id_title")
    private WebElement inputProblemTitle;

    @FindBy(id = "id_body")
    private WebElement ticketDescription;

    @FindBy(id = "id_due_date")
    private WebElement dueDateField;

    @FindBy(xpath = "//table[@class='ui_datapicker-calendar']//a[text()='23']")
    private WebElement dateValue;

    @FindBy(xpath = "//select[@id='id_priority']")
    private WebElement priorityField;

    @FindBy(xpath = "//select[@id='id_priority']//option[@value='2']")
    private WebElement priorityValue;

    @FindBy(id = "id_submitter_email")
    private WebElement userEmail;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement submitBtn;


    public CreateTicketPage() {
        // Необходимо инициализировать элементы класса, лучше всего это делать в конструкторе
        PageFactory.initElements(driver, this);
    }

    /** Создание тикета */
    public MainPage createTicket(Ticket ticket) {
        queueList.click();
        queueValue.click();
        setProblemTitle(ticket.getTitle());
        ticketDescription.sendKeys(ticket.getDescription());
        dueDateField.sendKeys(ticket.getDueDate());
        priorityField.sendKeys(String.valueOf(ticket.getPriority()));
        userEmail.sendKeys(ticket.getMail());
        createTicket();

        return new MainPage();
    }

    /** Заполнение поля "Summary of the problem" */
    public void setProblemTitle(String text) {
        inputProblemTitle.sendKeys(text);
    }


    /** Зажатие кнопки "Submit Ticket" */
    public void createTicket() {
        submitBtn.click();
    }

}
