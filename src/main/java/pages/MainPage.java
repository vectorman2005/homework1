package pages;

import elements.MainMenu;
import models.Ticket;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/** Главная страница */
public class MainPage extends HelpdeskBasePage {
    @FindBy(xpath = "//a[contains(@href,'/tickets/submit')]")
    private WebElement newTicketBtn;

    public MainPage() {
        PageFactory.initElements(driver, this);
    }

    public CreateTicketPage createNewTicket() {
        new MainMenu(driver).newTicket();
        return new CreateTicketPage();
    }

    public LoginPage goToLoginPage() {
        new MainMenu(driver).logIn();
        return new LoginPage();
    }

}
