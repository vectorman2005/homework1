package pages;

import models.Ticket;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/** Страница отдельного тикета */
public class TicketPage extends HelpdeskBasePage {

    @FindBy(xpath = "//h3")
    private WebElement ticketTitle;

    @FindBy(xpath = "//th[text()='Submitter E-Mail']")
    private WebElement email;

    @FindBy(xpath = "//th[text()='Due Date']")
    private WebElement dueDate;

    @FindBy(xpath = "//td[@id='ticket-description']//p")
    private WebElement description;

    @FindBy(xpath = "//th[text()='Priority']")
    private WebElement priority;

    public TicketPage() {
        PageFactory.initElements(driver, this);
    }

    /** Получить имя тикета */
    public String getTicketTitle() {
        return ticketTitle.getText();
    }

    public String getDescription() {
        return description.getText();
    }

    /** Получить адрес почты */
    public String getEmail() {
        // Получаем значение адреса почты
        return getValue(email);
    }

    public String getPriority() {
        return getValue(priority);
    }

    public String getDueDate() {
        return getValue(dueDate);
    }



    public Ticket buildTicket() {
        Ticket ticket = new Ticket();
        ticket.setTitle(getTicketTitle());
        ticket.setDescription(getDescription());
        ticket.setDueDate(getDueDate());
        ticket.setPriority(getPriority());
        ticket.setMail(getEmail());

        return ticket;
    }

    /**
     * Получить значение элемента таблицы
     *
     * @param columnElem элемент ячейки для которой нужно вернуть значение
     * @return текстовое значение ячейки рядом
     */
    private String getValue(WebElement columnElem) {
        return columnElem
                // Находим следующий элемент находящийся в том же теге
                .findElement(By.xpath("./following-sibling::td[1]"))
                // Получаем текст
                .getText()
                // Обрезаем лишние пробелы
                .trim();
    }

}
