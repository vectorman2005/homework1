package pages;

import elements.MainMenu;
import models.Ticket;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/** Страница со списком тикетов */
public class TicketsPage extends HelpdeskBasePage {
    @FindBy(xpath = "//tbody//tr//a")
    private List<WebElement> findedTickets;

    public TicketsPage() {
        PageFactory.initElements(driver, this);
    }

    /** Ищем строку с тикетом и нажимаем на нее */
    public TicketPage openTicket(Ticket ticket) {
        new MainMenu(driver).searchTicket(ticket);
        findedTickets.get(findedTickets.size() - 1).click();
        return new TicketPage();
    }

}
