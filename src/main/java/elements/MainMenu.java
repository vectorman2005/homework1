package elements;

import models.Ticket;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Элемент главного меню
 */
public class MainMenu {

    private WebDriver driver;

    @FindBy(xpath = "//a[contains(@href,'/tickets/submit')]")
    private WebElement newTicketBtn;

    @FindBy(id = "userDropdown")
    private WebElement loginBtn;

    @FindBy(id = "search_query")
    private WebElement searchInput;

    @FindBy(xpath = "//*[@id = 'searchform']//button[contains(@class, 'btn-primary')]")
    private WebElement searchSubmitBtn;


    public MainMenu(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void newTicket() {
        newTicketBtn.click();
    }

    public void logIn() {
        loginBtn.click();
    }

    public void searchTicket(Ticket ticket) {
        setSearch(ticket.getTitle())
                .search();
    }

    /* Если после вызова void метода, может потребоваться вызов другого метода этого же класса,
        то можно вернуть сам класс и вызвать следующий метод через точку. */
    public MainMenu setSearch(String text) {
        searchInput.sendKeys(text);
        return this;
    }

    public void search() {
        searchSubmitBtn.click();
    }


}
